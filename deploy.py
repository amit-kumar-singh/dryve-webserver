__author__ = ‘amit’

from fabric.api import sudo,env
env.user = 'ubuntu'
env.hosts.extend(['52.76.62.61'])
env.key_filename = 'tesigners.pem'

def deploy():
    sudo('cd /home/ubuntu/locomoco/;'
         'sudo git stash;'
         'sudo git fetch --progress --prune origin ;'
         ' sudo git merge --no-stat -v origin/master;'
         'git stash pop ;')
         # 'kill -HUP `cat /var/run/gunicorn.pid`;')