from django.conf import settings
from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpRequest, Http404
from django.template import RequestContext
from django.conf import settings
from django.core.urlresolvers import reverse
import pdb,logging, urllib2, urllib, sys, json,requests
from django.core.mail import EmailMultiAlternatives
from django.views.decorators.csrf import csrf_exempt
from rental.models import *

from payu.forms import PayUForm
from cart.forms import OrderForm
from payu.utils import verify_hash

from uuid import uuid4
from random import randint
from datetime import datetime

logger = logging.getLogger('django')

def value_from_req(request,key,default):
    value = getattr(request, 'GET').get(key)
    if not value:
        value = getattr(request, 'POST').get(key)
    if not value:
        return default
    return value

def getServerUrl():
    return settings.API_URL

def checkout(request):
    serverUrl = getServerUrl()
    if request.method == 'POST':

        vid = value_from_req(request,'vid','')
        user_id = value_from_req(request,'user','')
        pickup_time = value_from_req(request,'pickup_time','')
        drop_time = value_from_req(request,'drop_time','')
        pickup_location = value_from_req(request,'pickup_loc','')
        pr = value_from_req(request,'pr','')
        sd = value_from_req(request,'sd','')
        paymentId = value_from_req(request,'rpid','')

        duration = datetime.fromtimestamp(int(drop_time)/ 1e3) - datetime.fromtimestamp(int(pickup_time)/ 1e3)

        hr = float(duration.seconds)/3600
        days = float(duration.days)
        if hr>10:
            days = days+1
        else:
            days = days + (hr/10)

        amount = days * float(pr)
        minPrice = format((0.4 * float(pr)), '.2f')
        amount = format(amount, '.2f')

        if(float(amount) < float(minPrice)):
            amount = minPrice
        
        if vid!= '' and user_id!='' and pickup_time!='' and drop_time!='':

            initial = {}
            user_detail = getUserDetail(user_id)
            user_detail = json.loads(user_detail['data'])

            #save payment info
            payData = {
            "transaction_id":paymentId,
            "payment":amount
            }
            pay_info = savePaymentInfo(payData)

            if pay_info['status'] == "failed":

                msgType = 'alert-danger'
                msg='Payment failed.Please try again!'
                return render_to_response('searchVehicle.html',locals(),context_instance=RequestContext(request))

            pay_info = json.loads(pay_info['data inserted'])

            name = user_detail['name']
            email = user_detail['email']
            phone = user_detail['mobile_no']
            # update bookingdetail with payment info
            bookData = {
            'vehicle_id' : vid,
            'user_id' : user_detail['user_id'],
            'pickup_time' : pickup_time,
            'drop_time' : drop_time,
            'pickup_location' : pickup_location,
            'drop_location' : pickup_location,
            'total_fare' : amount,
            'payment_status' : "success",
            'payment_id' : pay_info['payment_id'],
            "doc_received" : "dl",
            "is_doc_received" : "false",
            "security_deposit":sd,
            "is_security_deposit":"true",
            "actual_drop_time":drop_time,
            "booking_system":1
            }

            booking_info = createUserBooking(bookData)
            vehicle_details = booking_info['vehicle']
            bikeName = booking_info['vehicle']['vehicle_name']
            booking_no = booking_info['booking_no']
            ptime = datetime.fromtimestamp(int(bookData['pickup_time'])/ 1e3).strftime('%d-%m-%Y %H:%M:%S')
            dtime = datetime.fromtimestamp(int(bookData['drop_time'])/ 1e3).strftime('%d-%m-%Y %H:%M:%S')
            mail_status = sendBookingConfirmationMail(bikeName,booking_no,name,email,ptime,dtime,phone,amount,vehicle_details['vendor']['vendor_address'],vehicle_details['vendor']['vendor_mob_no'],vehicle_details['vendor']['vendor_email'],booking_info['vehicle']['security_deposit'])

            return render_to_response('success.html',locals(),context_instance=RequestContext(request))
        else:
            logger.error('Something went wrong! Looks like booking form data validation is failing.')
            return HttpResponse(status=500)


def booking_detail(vid,ptime,rtime):
    bike_detail = AvailableVehicles.objects(vehicleName=vid).first()
    days = 2 #rtime - ptime
    amount = 2#bike_detail.tariff * days
    cart_detail = {
                    'bike':bike_detail,
                    'days':days,
                    'amt':amount
                }
    return cart_detail

@csrf_exempt
def success(request):
    if request.method == 'POST':
        if not verify_hash(request.POST):
            logger.warning("Response data for order (txnid: %s) has been "
                           "tampered. Confirm payment with PayU." %
                           request.POST.get('txnid'))
            #return redirect('order.failure')
            return render_to_response('failure.html',locals(),context_instance=RequestContext(request))
        else:
            logger.warning("Payment for order (txnid: %s) succeeded at PayU" %
                           request.POST.get('txnid'))

            #save payment info
            payData = {
            "transaction_id":request.POST.get('txnid'),
            "payment":request.POST.get('amount')
            }
            pay_info = savePaymentInfo(payData)
            pay_info = json.loads(pay_info['data inserted'])

            name = request.POST.get('firstname')
            email = request.POST.get('email')
            phone = request.POST.get('phone')
            # update bookingdetail with payment info
            bookData = {
            'vehicle_id' : request.POST.get('productinfo'),
            'user_id' : request.POST.get('udf1'),
            'pickup_time' : request.POST.get('udf2'),
            'drop_time' : request.POST.get('udf3'),
            'pickup_location' : request.POST.get('udf4'),
            'drop_location' : request.POST.get('udf4'),
            'total_fare' : request.POST.get('amount'),
            'payment_status' : request.POST.get('status'),
            'payment_id' : pay_info['payment_id'],
            "doc_received" : "dl",
            "is_doc_received" : "false",
            "security_deposit":1000,
            "is_security_deposit":"true",
            "actual_drop_time":request.POST.get('drop_time'),
            }

            amount = float(request.POST.get('amount'))
            pickup_loc = request.POST.get('udf4')
            booking_info = createUserBooking(bookData)
            vehicle_details = booking_info['vehicle']
            ptime = datetime.fromtimestamp(int(bookData['pickup_time'])/ 1e3).strftime('%d-%m-%Y %H:%M:%S')
            dtime = datetime.fromtimestamp(int(bookData['drop_time'])/ 1e3).strftime('%d-%m-%Y %H:%M:%S')
            sendMail(booking_info,name,email,ptime,dtime,phone,amount,vehicle_details['vendor']['vendor_address'],vehicle_details['vendor']['vendor_mob_no'],vehicle_details['vendor']['vendor_email'])

            return render_to_response('success.html',locals(),context_instance=RequestContext(request))
    else:
        raise Http404

@csrf_exempt
def failure(request):
    if request.method == 'POST':
        msgType = 'alert-danger'
        msg='Payment failed.Please try again!'
        return render_to_response('searchVehicle.html',locals(),context_instance=RequestContext(request))
    else:
        raise Http404

@csrf_exempt
def cancel(request):
    if request.method == 'POST':
        msgType = 'alert-danger'
        msg='Payment cancelled.Please try again!'
        return render_to_response('searchVehicle.html',locals(),context_instance=RequestContext(request))
    else:
        raise Http404

def checkout1(request):
    if request.method == 'POST':

        vid = value_from_req(request,'vid','')
        user_id = value_from_req(request,'user','')
        pickup_time = value_from_req(request,'pickup_time','')
        drop_time = value_from_req(request,'drop_time','')
        pickup_location = value_from_req(request,'pickup_loc','')
        pr = value_from_req(request,'pr','')
        paymentId = value_from_req(request,'rpid','')

        #a = datetime.strptime(pickup_time, "%Y-%m-%d %H:%M")
        #b = datetime.strptime(drop_time, "%Y-%m-%d %H:%M")
        duration = datetime.fromtimestamp(int(drop_time)/ 1e3) - datetime.fromtimestamp(int(pickup_time)/ 1e3)

        hr = float(duration.seconds)/3600
        days = float(duration.days)
        if hr>10:
            days = days+1
        else:
            days = days + (hr/10)

        amount = days * float(pr)
        minPrice = 0.4 * float(pr)

        if(amount < minPrice):
            amount = minPrice

        if vid!= '' and user_id!='' and pickup_time!='' and drop_time!='':

            initial = {}
            user_detail = getUserDetail(user_id)
            user_detail = json.loads(user_detail['data'])

            initial.update({'key': settings.PAYU_INFO['merchant_key'],
                            'txnid': uuid4().hex,
                            'surl': request.build_absolute_uri(reverse('order.success')),
                            'furl': request.build_absolute_uri(reverse('order.failure')),
                            'curl': request.build_absolute_uri(reverse('order.cancel')),
                            'service_provider': settings.PAYU_INFO['service_provider'],
                            'firstname': user_detail['name'],
                            'email': user_detail['email'],
                            'phone': user_detail['mobile_no'],
                            'productinfo':vid,
                            'udf1':user_detail['user_id'],
                            'udf2':pickup_time,
                            'udf3':drop_time,
                            'udf4':pickup_location,
                            'amount':amount
                            })
            # Once you have all the information that you need to submit to payu
            # create a payu_form, validate it and render response using
            # template provided by PayU.

            payu_form = PayUForm(initial)

            if payu_form.is_valid():
                context = {'form': payu_form,
                           'action': "%s" % settings.PAYU_INFO['payment_url']}
                return render(request, 'payu_form.html', context)
            else:
                logger.error('Something went wrong! Looks like initial data used for payu_form is failing validation')
                return HttpResponse(status=500)
        else:

            vid = value_from_req(request,'vid','')

            ptime = value_from_req(request,'ptime','')
            rtime = value_from_req(request,'rtime','')

            cart = booking_detail(vid,ptime,rtime)

            initial = { 'txnid': uuid4().hex,
                        'productinfo': vid,
                        'amount': cart['amt']}
            order_form = OrderForm(initial=initial)

    context = {'form': order_form, 'vid':vid, 'cart':cart}
    #return render_to_response('checkout.html',locals(),context_instance=RequestContext(request))
    return render(request, 'checkout.html',context)

def getUserDetail(user_id):
    url = getServerUrl()+'/Dryve/user/get/email?email='+user_id
    result = urllib.urlopen(url, user_id)
    content = result.read()
    return json.loads(content)

def createUserBooking(bookData):
    url = getServerUrl()+'/Dryve/booking/create'
    req = urllib2.Request(url)
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req, json.dumps(bookData))

    #result = urllib.urlopen(url, json.dumps(bookData),{"Content-type": "application/json"})
    content = response.read()
    return json.loads(content)

def savePaymentInfo(payData):
    url = getServerUrl()+'/Dryve/user/payment'
    req = urllib2.Request(url)
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req, json.dumps(payData))
    #result = urllib.urlopen(url, json.dumps(bookData),{"Content-type": "application/json"})
    content = response.read()
    return json.loads(content)

def sendBookingConfirmationMail(bikeName,booking_no,name,email,ptime,dtime,phone,amount,vendor_add,vendor_mob,vendor_email,security_deposit):
    url = getServerUrl()+'/Dryve/booking/success/email?'

    url+= 'booking_no='+booking_no+'&vehicle_name='+bikeName+'&name='+name+'&email='+email+'&ptime='+ptime+'&dtime='+dtime+'&phone='+str(phone)
    url+= '&amount='+str(amount)+'&vendor_add='+vendor_add+'&vendor_mob='+str(vendor_mob)+'&vendor_email='+vendor_email+'&security_deposit='+str(security_deposit)

    result = urllib.urlopen(url, booking_no)

    content = result.read()
    return content

def sendMail(booking_info,name,email,ptime,dtime,phone,amount,vendor_add,vendor_mob,vendor_email):

    bikeName = ''+booking_info['vehicle']['vehicle_name']
    booking_no = ''+booking_info['booking_no']

    message = 'Hi '+name+ ',\r\rGreetings from Dryve!\r\rWe thank you for your booking.\rPlease find below, the summary of your order number - '+booking_no+'\r\r'
    message = message +'Name: '+name+'\rMobile Number: '+phone
    message = message +'\r\rVehicle Details: '+bikeName+'\rPickup Time: '+ptime+'\rDrop Time: '+dtime
    message = message +'\rPickup Location: '+vendor_add+'\rVendor contact number: '+vendor_mob
    message = message +'\r\rRental Charges: Rs. '+str(amount)+'\rSecurity Deposit to be paid: Rs. 1000'
    message = message + '\r\rFor any queries, you may call us at +91-7899861150\r\rRegards\rTeam Dryve\rwww.dryve.co.in'
    subject = 'Dryve Booking Confirmation - Your booking with Dryve.co.in has been successfully placed'
    to = [email]
    bcc = [vendor_email,'support@dryve.co.in']
    email = EmailMultiAlternatives(subject, message, to=to, bcc=bcc)
    #email.attach_alternative(html_content, "text/html")
    email.send()
