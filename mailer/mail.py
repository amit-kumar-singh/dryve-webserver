__author__ = 'prabhat'
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime


class Mailer(object):

    def __init__(self,**kwargs):
        self.from_email = kwargs.get('from','admin@snipp.in')
        self.to_email = kwargs.get('to',[])
        self.subject = kwargs.get('subject','snipp.com')
        self.order_detail = kwargs.get('order_detail',[])
        self.shipping_detail = kwargs.get('shipping_detail','')

        self.html_part = kwargs.get('html_part','')
        # if self.html_part == 'user_mail':
        #     self.html_part = user_orderconfirm_email_content(self.order_detail,self.shipping_detail)

        self.plain_part = kwargs.get('plain_part','')
        self.reply_to = kwargs.get('reply-to','')


    def send_mail(self):

        text = self.plain_part
        html = self.html_part
        s = smtplib.SMTP()
        s.connect()
        # s = smtplib.SMTP('email-smtp.us-west-2.amazonaws.com','587')
        # s.ehlo()
        # s.starttls()
        # s.ehlo
        # s.login('AKIAJJXB3VCT46XYXWWQ','AhunorCvTR0MNbeG5LB4n+trtmQRgPc57q62q6AXF+Dm')
        # and message to send - here it is sent as one string.
        if self.html_part:
            # Create message container - the correct MIME type is multipart/alternative.
            msg = MIMEMultipart('alternative')
            # Record the MIME types of both parts - text/plain and text/html.
            part1 = MIMEText(text, 'plain')
            part2 = MIMEText(html, 'html')
            # Attach parts into message container.
            # According to RFC 2046, the last part of a multipart message, in this case
            # the HTML message, is best and preferred.
            msg.attach(part1)
            msg.attach(part2)
        else:
            msg = MIMEMultipart()
            part1 = MIMEText(text, 'plain')
            msg.attach(part1)
        if self.reply_to:
            msg.add_header('reply-to', self.reply_to)
        msg['Subject'] = self.subject
        msg['From'] = "<"+self.from_email+">"
        msg['To'] = ", ".join(self.to_email)
        s.sendmail(self.from_email, self.to_email, msg.as_string())
        s.quit()