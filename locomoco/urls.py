from django.conf.urls import patterns, include, url
from django.conf import settings
from rental.views import *
from django.shortcuts import render_to_response

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'website.views.home', name='home'),
    # url(r'^website/', include('website.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),
    url(r'^$',index),
    (r'^order/', include('cart.urls')),
    url(r'^send_feedback/$',save_booking),
    url(r'^bookings/get_booking_enquiry/$',booking),
    url(r'^rent-bike-in/(?P<location>[a-z,0-9,-]+)/$', getLocationPageTemplate),
    url(r'^rent-bike/(?P<bikeName>[\a-z,0-9,-, \+]+)/$', getBikePageTemplate),
    url(r'^search/filter/$',search_filter),
    url(r'^search/$',search_bikes),
    url(r'^login/$',login_user),
    url(r'^logout/$',logout_vendor),
    url(r'^contact-us/$',getContactPage),
     url(r'^bike-tariff/$',getTariffTable),
    url(r'^policy/(?P<policy>[\a-z,0-9,-, \+]+)/$',getPolicyPage),
    (r'^robots.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: ", content_type="text/plain")),
    url(r'^sitemap.xml$',getSitemap),
    url(r'^bookings/block-bike/$',blockBike),
    url(r'^bookings/home/$', process_login),
    url(r'^bookings/reserve-bike/$',reserveBikeForVendor),
    url(r'^bookings/get_booking_enquiry/cancel-booking/$',cancel_booking),
    url(r'^bookings/get_booking_enquiry/modify-booking/$',modify_booking),

)
