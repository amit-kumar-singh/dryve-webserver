from datetime import datetime
from django import template
register = template.Library()

@register.filter("timestamp")
def timestamp(value):
    try:
        return datetime.fromtimestamp(int(value)/ 1e3).strftime('%Y-%m-%d %H:%M')
    except AttributeError:
        return ''
