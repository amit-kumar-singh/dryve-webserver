from mongoengine import *
from django.conf import settings
from time import time
from slugify import slugify
from django.contrib.auth.hashers import check_password, make_password


class Booking(Document):
    name                      = StringField(db_field='name',required=False)
    email                     = StringField(db_field='email',required=False)
    phone                     = StringField(db_field='phone',required=False)
    comment                   = StringField(db_field='comt',required=False)
    created_at                = DateTimeField(db_field='ct',required=False)
    updated_at                = DateTimeField(db_field='ut',required=False)
    
class User(Document):
    email_id                  = StringField(db_field='eid',required=False,default='')
    password                  = StringField(db_field='pass',required=False)
    signup_source             = StringField(db_field='sic',required=False)
    signup_source_id          = StringField(db_field='siid',required=False)
    img_url                   = StringField(db_field='si',required=False)
    large_image               = StringField(db_field='li',required=False)
    first_name                = StringField(db_field='fn',required=False)
    last_name                 = StringField(db_field='ln',required=False)
    name                      = StringField(db_field='n',required=False)
    location                  = DictField(db_field='loc',required=False)
    address                   = ListField(db_field='add',required=False)
    gender                    = StringField(db_field='gen',required=False)
    birthday                  = StringField(db_field='bd',required=False)
    created_at                = IntField(required = False, db_field='ct',default=int(time()))
    updated_at                = IntField(required = False , db_field='ut',default=int(time()))


    def set_password(self,password):
        self.password = make_password(password)
        self.save()
        return self

    def check_password(self,password):
        return check_password(password,self.password)

    def save(self, force_insert=False, validate=True, clean=True,
             write_concern=None,  cascade=None, cascade_kwargs=None,
             _refs=None, **kwargs):
        if not self.id:
            self.created_at = int(time())
        self.updated_at = int(time())
        super(Users, self).save(force_insert=False, validate=True, clean=True,
             write_concern=None,  cascade=None, cascade_kwargs=None,
             _refs=None, **kwargs)

    def get_details(self):
        user_data = {
            'email_id':self.email_id,
            'small_img':self.img_url,
            'name':self.name,
            'id':str(self.id)
        }
        return user_data


class Transaction(Document):
    user                      = ReferenceField(User,db_field='usr',required=True)
    created_at                = DateTimeField(db_field='ct',required=False)
    updated_at                = DateTimeField(db_field='ut',required=False)


class AvailableVehicles(Document):
    vehicleName               = StringField(db_field='name',required=False)
    tariff                    = IntField(db_field='tar',required=False)
    advance_amt               = IntField(db_field='adv',required=False)
    img_url                   = StringField(db_field='img',required=False)
    pickup_time               = StringField(db_field='stime',required=False)
    return_time               = StringField(db_field='etime',required=False)
    color                     = StringField(db_field='color',required=False)
    vehicle_id                = StringField(db_field='vid',required=False)

    def getAvailableVehicles(self):
        dict = {
            'vehicleName' : self.vehicleName if self.vehicleName else '',
            'tariff' : self.tariff,
            'advance' : self.advance_amt,
            'img_url' : self.img_url,
            'color' : self.color,
            'vid' : str(self.vehicle_id),
            'id' : str(self.id)
        }
        return dict

class Bookings(Document):
    user                      = ReferenceField(User,db_field='usr',required=True)
    transaction               = ReferenceField(Transaction,db_field='btr',required=True)
    vehicle                   = ReferenceField(AvailableVehicles,db_field='bvh',required=True)
    pickup_time               = DateTimeField(db_field='bpt',required=False)
    drop_time                 = DateTimeField(db_field='bdt',required=False)
    actual_drop_time          = DateTimeField(db_field='badt',required=False)
    pickup_location           = StringField(db_field='bpl',required=False)
    drop_location             = StringField(db_field='bdl',required=False)
    payment_status            = BooleanField(db_field='bps',required=False)
    security_deposit          = FloatField(db_field='bsd',required=False)
    is_security_deposit       = BooleanField(db_field='bid',required=False)
    doc_received              = StringField(db_field='bdr',required=False)
    is_doc_received           = BooleanField(db_field='bisr',required=False)
    total_fare                = FloatField(db_field='btf',required=False)
    created_at                = DateTimeField(db_field='ct',required=False)
    updated_at                = DateTimeField(db_field='ut',required=False)


class Brand(Document):
    brand_name                = StringField(db_field='brn',required=False)
    brand_pic                 = StringField(db_field='brp',required=False)
    variant                   = StringField(db_field='brv',required=False)
    year                      = IntField(db_field='bry',required=False)
    engine_capacity           = StringField(db_field='brec',required=False)
    created_at                = DateTimeField(db_field='ct',required=False)
    updated_at                = DateTimeField(db_field='ut',required=False)
