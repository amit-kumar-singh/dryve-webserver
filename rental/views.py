import json
import base64
from django.conf import settings
from django.http import HttpResponse,HttpResponsePermanentRedirect,HttpResponseNotFound
from django.shortcuts import render_to_response
from django.template import RequestContext
from rental.models import *
from django.core.mail import EmailMultiAlternatives
from mailer.mail import Mailer
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from django.template.defaultfilters import slugify
import pdb,logging, urllib2, urllib, sys, json
from uuid import uuid4


def value_from_req(request,key,default):
    value = getattr(request, 'GET').get(key)
    if not value:
        value = getattr(request, 'POST').get(key)
    if not value:
        return default
    return value

def getServerUrl():
    return settings.API_URL

def index(request):
    return render_to_response('index.html',locals(),context_instance=RequestContext(request))

@csrf_exempt
def login_user(request):
    return render_to_response('login.html',locals(),context_instance=RequestContext(request))

def getVendorId(email):
    url = getServerUrl()+'/Dryve/vendor/get/email?email='+email

    result = urllib.urlopen(url, email)
    content = result.read()
    vendor = json.loads(content)
    if vendor['status']=="success":
        vendorData = json.loads(vendor['data'])
        return vendorData['vendor_id']
    else:
        return -1


def verify_login(uname,password):
    url = getServerUrl()+'/Dryve/user/signin?email='+uname+'&password='+password
    result = urllib.urlopen(url, uname)
    content = result.read()
    user_res = json.loads(content)
    return user_res

def logout_vendor(request):
    response = render_to_response('login.html',locals(),context_instance=RequestContext(request))
    response.delete_cookie('sid')
    return response

@csrf_exempt
def process_login(request):
    serverUrl = getServerUrl()
    sid = ''
    uid=''
    vendor_id=''
    if 'sid' in request.COOKIES:
        value = request.COOKIES['sid']
        sid = value.split('_')
        uid = sid[0]
        vendor_id = sid[1]
        return render_to_response('VendorDashboardHome.html',locals(),context_instance=RequestContext(request))
    elif request.method=='POST':
        uname = value_from_req(request,'user','')
        password = value_from_req(request,'password','')
        user_res = verify_login(uname,password)
        if user_res['status']=="success":
            user_data = json.loads(user_res['data:- '])
            u_id = user_data['user_id']
            vendor_id = getVendorId(uname)

            if vendor_id == -1:
                msg='User is not a valid vendor.Please try vendor login credentials!'
                return render_to_response('login.html',locals(),context_instance=RequestContext(request))
            elif vendor_id > 0:
                response = render_to_response('VendorDashboardHome.html',locals(),context_instance=RequestContext(request))
                session_id = str(u_id)+'_'+str(vendor_id)
                response.set_cookie('sid', session_id,max_age=3600)     # session age= 1 hour
                return response
        else:
            msg='Login information is incorrect.Please try again!'
            return render_to_response('login.html',locals(),context_instance=RequestContext(request))
    else:
        msg='Please login!'
        return render_to_response('login.html',locals(),context_instance=RequestContext(request))


def vendor_home(request):
    serverUrl = getServerUrl()
    sid = ''
    u_id=''
    v_id=''
    if 'sid' in request.COOKIES:
        value = request.COOKIES['sid']
        sid = value.split('_')
        u_id = sid[0]
        v_id = sid[1]
        return render_to_response('VendorDashboardHome.html',locals(),context_instance=RequestContext(request))
    else:
        msg='Please login!'
        return render_to_response('login.html',locals(),context_instance=RequestContext(request))

def getTariffTable(request):
    return render_to_response('tariffTable.html',locals(), context_instance=RequestContext(request))

def getSitemap(request):
    res = render_to_response('sitemap.xml',locals(),context_instance=RequestContext(request))
    res['Content-Type'] = 'text/xml'
    return res

def getPolicyPage(request,policy):
    policyName = ''+ policy +'.html'
    return render_to_response(policyName,locals(),context_instance=RequestContext(request))

def checkOut(request):
    return render_to_response('checkout1.html', locals(), context_instance=RequestContext(request))

def getContactPage(request):
    return render_to_response('contactUs.html', locals(), context_instance=RequestContext(request))

def getLocationPageTemplate(request,location):
    loc = 'location//'+ location +'.html'
    return render_to_response(loc,locals(),context_instance=RequestContext(request))

def getBikePageTemplate(request,bikeName):
    loc = 'bikes//'+ slugify(bikeName) +'.html'
    return render_to_response(loc,locals(),context_instance=RequestContext(request))

def cancel_booking(request):
    booking_no = value_from_req(request,'selected_row','')
    url = getServerUrl()+'/Dryve/booking/modify'
    cancelBookingData = {
    'booking_no' : booking_no,
    'booking_status' : 0
    }
    req = urllib2.Request(url)
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req, json.dumps(cancelBookingData))
    content = response.read()
    return HttpResponseRedirect("/bookings/get_booking_enquiry/")

def modify_booking(request):

    booking_no = value_from_req(request,'bookingNo','')
    vid = value_from_req(request,'vid','')
    amt = value_from_req(request,'amt','')
    stime = value_from_req(request,'stime','')
    etime = value_from_req(request,'etime','')

    url = getServerUrl()+'/Dryve/booking/modify'

    modifyBookingData = {
        'booking_no':booking_no,
        'pickup_time':stime,
        'drop_time':etime,
        'vehicle_id':vid,
        'total_fare':amt
        }
    req = urllib2.Request(url)
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req, json.dumps(modifyBookingData))
    content = response.read()
    return HttpResponseRedirect("/bookings/get_booking_enquiry/")


def blockBike(request):
    serverUrl = getServerUrl()
    sid = ''
    u_id=''
    v_id=''
    if 'sid' in request.COOKIES:
        value = request.COOKIES['sid']
        sid = value.split('_')
        u_id = sid[0]
        v_id = sid[1]
    if u_id != '' and v_id != '':
        return render_to_response('inventory/blockBike.html',locals(),context_instance=RequestContext(request))
    else:
        msg='Please login!'
        return render_to_response('login.html',locals(),context_instance=RequestContext(request))


def reserveBikeForVendor(request):
    serverUrl = getServerUrl()
    sid = ''
    uid=''
    vendor_id=''

    if 'sid' in request.COOKIES:
        value = request.COOKIES['sid']
        sid = value.split('_')
        uid = sid[0]
        vendor_id = sid[1]

    if uid != '' and vendor_id != '':

        vid = value_from_req(request,'vid','')
        pickup_time = value_from_req(request,'pickup_time','')
        drop_time = value_from_req(request,'drop_time','')
        pickup_location = value_from_req(request,'pickup_loc','')
        amount = value_from_req(request,'am',0)
        user_id = uid #value_from_req(request,'u_id','')
        #vendor_id = value_from_req(request,'v_id','')
        cName = value_from_req(request,'cname','')
        mp_name = value_from_req(request,'mp_name','')
        cEmail = request.POST.get('cEId')
        cPhone = request.POST.get('cPhone')
        vendors_user_id = ''

        if vid =='' or pickup_time =='' or drop_time=='':
            return HttpResponse(json.dumps({'status':False}))

        reqFromSubmitBtn = value_from_req(request,'fromSubmit','')

        if reqFromSubmitBtn != '1':

            return render_to_response('inventory/blockBike.html',locals(),context_instance=RequestContext(request))
        else:
            if cName =='' or cEmail =='':
                vendors_user_id = ''

            else:

                userData = {
                'name' : cName,
                'email' : cEmail,
                'mobile_no' : cPhone,
                'address' : request.POST.get('addr'),
                "marketplace_name":mp_name
                }

                # add customer to vendors marketplace table
                url = getServerUrl()+'/Dryve/vendor/user/create'
                req = urllib2.Request(url)
                req.add_header('Content-Type', 'application/json')

                response = urllib2.urlopen(req, json.dumps(userData))
                content = response.read()
                user_res = json.loads(content)
                user_detail = json.loads(user_res['data'])
                vendors_user_id = user_detail['vendors_user_id']

            paymentData = {
            'txnid': uuid4().hex,
            'payment':amount
            }
            url = getServerUrl()+'/Dryve/user/payment/offline'
            req = urllib2.Request(url)
            req.add_header('Content-Type', 'application/json')
            response = urllib2.urlopen(req, json.dumps(paymentData))
            content = response.read()
            payment_detail = json.loads(content)
            pay_info = json.loads(payment_detail['data inserted'])


            if pay_info['payment_id']:

                bookData = {
                'vehicle_id' : vid,
                'user_id' : user_id,
                'pickup_time' : request.POST.get('pickup_time'),
                'drop_time' : request.POST.get('drop_time'),
                'pickup_location' : request.POST.get('pickup_loc'),
                'drop_location' : request.POST.get('pickup_loc'),
                'total_fare' : amount,
                'payment_status' : 'true',
                'payment_id' : pay_info['payment_id'],
                'doc_received' : "dl",
                'is_doc_received' : "false",
                'security_deposit':0,
                'is_security_deposit':"false",
                'actual_drop_time':request.POST.get('drop_time'),
                'booking_purpose':request.POST.get('reason'),
                'vendor_manual_booking':'true'
                #'vendors_user_id':vendors_user_id
                }

                if vendors_user_id !='':
                    bookData['vendors_user_id'] = vendors_user_id

                url = getServerUrl()+'/Dryve/booking/create'
                req = urllib2.Request(url)
                req.add_header('Content-Type', 'application/json')
                response = urllib2.urlopen(req, json.dumps(bookData))
                content = response.read()
                booking_info = json.loads(content)


                if booking_info['booking_no']:

                    msgType = "alert alert-success"
                    msg='Bike booked successfully with Booking Number - '+booking_info['booking_no']
                    #bookingList = booking(request)
                    #return render_to_response('booking_enquiry.html',locals(),context_instance=RequestContext(request))
                    return HttpResponseRedirect("/bookings/get_booking_enquiry/")


    else:
        msg='Please login!'
        return render_to_response('login.html',locals(),context_instance=RequestContext(request))
        #return render_to_response('booking_enquiry.html',locals(),context_instance=RequestContext(request))


@csrf_exempt
def booking(request):
    serverUrl = getServerUrl()
    #booking_enquiry = Booking.objects.order_by('-created_at')
    sid = ''
    uid=''
    vendor_id=''
    if 'sid' in request.COOKIES:
        value = request.COOKIES['sid']
        sid = value.split('_')
        uid = sid[0]
        vendor_id = sid[1]

    if request.method=='POST' or sid != '':
        uname = value_from_req(request,'user','')
        password = value_from_req(request,'password','')

        if uid == '' and vendor_id == '':

            user_res = verify_login(uname,password)
            if user_res['status']=="success":

                user_data = json.loads(user_res['data:- '])
                user_id = user_data['user_id']
                vendor_id = getVendorId(uname)

                if vendor_id == -1:

                    msg='User is not a valid vendor.Please try vendor login credentials!'
                    return render_to_response('login.html',locals(),context_instance=RequestContext(request))
                elif vendor_id > 0:

                    url = getServerUrl()+'/Dryve/booking/history/all?vendorId='+str(vendor_id)
                    result = urllib.urlopen(url, str(vendor_id))
                    content = result.read()
                    bookingList = json.loads(content)

                    response = render_to_response('booking_enquiry.html',locals(),context_instance=RequestContext(request))
                    session_id = str(user_id)+'_'+str(vendor_id)
                    response.set_cookie('sid', session_id,max_age=3600)     # session age= 1 hour
                    return response
            else:

                msg='Login information is incorrect.Please try again!'
                return render_to_response('login.html',locals(),context_instance=RequestContext(request))

        elif uid != '' and vendor_id != '':

            if vendor_id == -1:
                msg='User is not a valid vendor.Please try vendor login credentials!'
                return render_to_response('login.html',locals(),context_instance=RequestContext(request))
            else:

                url = getServerUrl()+'/Dryve/booking/history/all?vendorId='+str(vendor_id)
                result = urllib.urlopen(url, str(vendor_id))
                content = result.read()
                bookingList = json.loads(content)

                return render_to_response('booking_enquiry.html',locals(),context_instance=RequestContext(request))

    else:
        msg='Please login!'
        return render_to_response('login.html',locals(),context_instance=RequestContext(request))

def search_bikes(request):
    serverUrl = getServerUrl()
    return render_to_response('searchVehicle.html',locals(),context_instance=RequestContext(request))

def search_filter(request):
    startTime = value_from_req(request,'startTime','')
    endTime   = value_from_req(request,'endTime'  ,'')
    bikeName  = value_from_req(request,'bikeName' ,'')
    if bikeName == 'All Bikes':
        bikeName = ''

    bikeList = []
    filterString = ''

    if startTime !='':
        filterString = 'pickup_time="'+startTime+'"'

    if endTime !='':
        if filterString !='':
            filterString += ',return_time="'+endTime+'"'
        else:
            filterString = 'return_time="'+endTime+'"'

    if bikeName !='':
        if filterString !='':
            filterString += ',vehicleName="'+bikeName+'"'
        else:
            filterString = 'vehicleName="'+bikeName+'"'

    if bikeName == '':
        for bike in AvailableVehicles.objects.filter():
            bikeList.append(bike.getAvailableVehicles())
    else:
        for bike in AvailableVehicles.objects.filter(vehicleName= bikeName):
            bikeList.append(bike.getAvailableVehicles())

    return HttpResponse(json.dumps({'vehicles':bikeList, 'status':True}))

def save_booking(request):
    name = value_from_req(request,'name','')
    email = value_from_req(request,'email','')
    phone = value_from_req(request,'phone','')
    comment = value_from_req(request,'comment','')

    if name == '' or email == '' or phone == '':
        return HttpResponse(json.dumps({'status':False}))

    b = Booking(name=name)
    b.email = email
    b.phone = phone
    b.comment = comment
    b.created_at = datetime.now()
    b.updated_at = datetime.now()
    b.save()

    message = 'Hi,\r\rWe have received a feedback from '+name+ '\r'
    message = message + '\rName: '+name+'\remail: '+email+'\rphone: '+phone+'\rMessage: '+comment+' \r\rRegards\rTeam Dryve\rwww.dryve.co.in'

    subject = 'Dryve user feedback received'
    to = [email]
    bcc = ['support@dryve.co.in']

    #html_content = '<p>Hi <strong>'+name+'</strong><br><br>Greetings from Dryve!</p>'

    #email = EmailMessage(subject, message, to=to, bcc=bcc)
    #email = EmailMultiAlternatives(subject, message, to=to, bcc=bcc)
    email = EmailMultiAlternatives(subject, message, to=bcc)
    #email.attach_alternative(html_content, "text/html")
    email.send()

    #maildata = {'from':'amit07sun@gmail.com',
    #         'to':['iamanmittal@gmail.com,tesigners.lab@gmail.com,amit07sun@gmail.com'],
    #         'subject':'New booking',
    #         'plain_part':message,
    #         'html_part':''
    #         }
    #mail = Mailer(**maildata)
    #mail.send_mail()

    return HttpResponse(json.dumps({'status':True}))
